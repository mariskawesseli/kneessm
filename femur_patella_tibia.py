# -*- coding: utf-8 -*-
"""
====================================================================
Full Example Pipeline for Statistical Shape Modeling with ShapeWorks
====================================================================

In this example we provide a full pipeline with optimizing on femur meshes.
"""

import os
import re
import numpy as np
from GroomUtils import *
from OptimizeUtils_domains import *
from AnalyzeUtils import *
import CommonUtils
import shutil

def concatenate_particle_files(file_type,domains_per_shape,outputDirectory,shapeFolder):

	# """ Get all files of either the world co-ordinates or local """
	fileList= []
	for file in glob.glob(outputDirectory+"*"+file_type+"*"):
		fileList.append(file)

	num_shapes = int(len(fileList)/domains_per_shape)
	
	#""" Initialize the combined files name list """
	outfile_list = ''

	#""" New folder for the concatinated files """
	combinedDirectory = outputDirectory+"combined/"
	if not os.path.exists(combinedDirectory):
		os.makedirs(combinedDirectory)


	#""" Concate the files """
	for i in range(num_shapes-1):
		if i<8:
			ind = i+1
		else:
			ind = i + 2

		output_filename = combinedDirectory + "joint_" + str(ind).zfill(2) + "_" + file_type+".particles"
		outfile_list += output_filename+"\n"
		with open(output_filename,'w',encoding='ascii') as out:
			for j in ['FEM','TIB','PAT']:
				
				#in_file = glob.glob(outputDirectory + str(ind) + "_" + j + "*"+file_type+"*")[0]
				in_file = glob.glob(outputDirectory + "*" + j + "_" + str(ind) + "*"+file_type+"*")[0]
				#print(in_file)
				# if j is 'PAT':
				# 	data = np.genfromtxt(in_file, delimiter=" ")
				# 	data[:, 0] = data[:, 0] - 53
				# 	#data[:, 1] = data[:, 1] - 5
				# 	#data[:, 2] = data[:, 2] - 4
				# 	np.savetxt(in_file, data, delimiter=" ")
				if j is 'TIB':
					data = np.genfromtxt(in_file, delimiter=" ")
					#data[:, 0] = data[:, 0] + 53
					data[:, 1] = data[:, 1] - 40
					#data[:, 2] = data[:, 2] - 4
					np.savetxt(in_file, data, delimiter=" ")
				with open(in_file,'r',encoding='ascii') as inp:
				
					shutil.copyfileobj(inp,out)

	#""" Save the output file list in the xml file which will be passed to the Studio
	xmlfilename = shapeFolder + "multi_domain_"+file_type+".xml"
	xml = open(xmlfilename,"w")
	xml.write("<?xml version=\"1.0\" ?>\n")
	xml.write("<domains_per_shape>\n" + str(domains_per_shape) + "\n</domains_per_shape>\n")
	xml.write("<point_files>\n" + outfile_list +"\n</point_files>")
	xml.close()


def copytree(src, dst, symlinks=False, ignore=None):
	for item in os.listdir(src):
		s = os.path.join(src, item)
		d = os.path.join(dst, item)
		if os.path.isdir(s):
			try:
				shutil.copytree(s, d, symlinks, ignore)
			except:
				pass
		else:
			try:
				shutil.copy2(s, d)
			except:
				pass


def Run_Pipeline(args):
	"""
	# locate data
	"""

	datasetName = "femur_patella_tibia"
	outputDirectory = "Output/femur_patella_tibia/"
	if not os.path.exists(outputDirectory):
		os.makedirs(outputDirectory)

	"""
	## GROOM : Data Pre-processing
	For the unprepped data the first few steps are
	-- Reflect images and meshes
	-- Turn meshes to volumes
	-- Isotropic resampling
	-- Padding
	-- Center of Mass Alignment
	-- Centering
	-- Rigid Alignment
	-- if interactive tag and option 2 was chosen - define cutting plane on mean sample
	-- clip segementations with cutting plane
	-- find largest bounding box and crop
	"""
	# Directory where grooming output folders will be added
	groomDir = outputDirectory + 'groomed/'
	if not os.path.exists(groomDir):
		os.makedirs(groomDir)

	# set name specific variables
	img_suffix = "1x_pat"
	reference_side = "left"
	inputDir = outputDirectory + '/'

	if args.start_with_image_and_segmentation_data:
		print("\n\n************************ WARNING ************************")
		print("'start_with_image_and_segmentation_data' tag was used \nbut  data set does not have images.")
		print("Continuing to run with segmentations only.")
		print("*********************************************************\n\n")

	if int(args.start_with_prepped_data) == 1:
		print("Skipping grooming...")
		dtFiles = []
		dt_dir = outputDirectory + '/groomed/distance_transforms/'
		for file in sorted(os.listdir(dt_dir)):
			dtFiles.append(dt_dir + file)

		myorder = []
		for i in range(0,49):
			myorder.append(i)
			myorder.append(i + 50)
			myorder.append(i + 100)
		dtFiles = [dtFiles[i] for i in myorder]
		if args.tiny_test:
			dtFiles = dtFiles[:3]

	else:
		for j in ['FEM','PAT','TIB']:  #
			files_mesh = []
			# Get mesh segmentation file lists
			mesh_dir = inputDir + 'meshes/'
			for file in glob.glob(mesh_dir + "*_" + j + ".ply"):
				files_mesh.append(file)
			groomDir_dm = outputDirectory + j + '/'
			
			"""
			Reflect - We have left and right femurs, so we reflect the non-reference side meshes so that all of the femurs can be aligned
			"""
			reflectedFiles_mesh = reflectMeshes(groomDir_dm + 'reflected', files_mesh, reference_side)

			"""
			MeshesToVolumes - Shapeworks requires volumes so we need to convert 
			mesh segementaions to binary segmentations.
			"""
			# set spacing
			spacing = [1, 1, 1]
			# answer = input("Use ispotropic spacing for mesh rasterization? y/n \n")
			# if answer == 'n':
			#	done = False
			#	while not done:
			#		spacing = []
			#		spacing.append(float(input("Enter x spacing:\n")))
			#		spacing.append(float(input("Enter y spacing:\n")))
			#		spacing.append(float(input("Enter z spacing:\n")))
			#		answer2 = input('Is spacing = ' + str(spacing) + ' okay? y/n\n')
			#		if answer2 == 'y':
			#			done = True

			fileList_seg = MeshesToVolumes(groomDir_dm + "volumes", reflectedFiles_mesh, spacing)

			"""
			Apply isotropic resampling
			The segmentation and images are resampled independently to have uniform spacing.
			"""
			resampledFiles_segmentations = applyIsotropicResampling(groomDir_dm + "resampled/segmentations", fileList_seg,
																	isBinary=True)

			"""
			Apply padding
			Both the segmentation and raw images are padded in case the seg lies on the image boundary.
			"""
			paddedFiles_segmentations = applyPadding(groomDir_dm + "padded/segementations", resampledFiles_segmentations, 10)

			"""
			Apply center of mass alignment
			This function can handle both cases (processing only segmentation data or raw and segmentation data at the same time).
			"""
			comFiles_segmentations = applyCOMAlignment(groomDir_dm + "com_aligned", paddedFiles_segmentations, None)

			"""
			Apply centering
			"""
			centerFiles_segmentations = center(groomDir_dm + "centered/segmentations", comFiles_segmentations)

			"""
			Rigid alignment needs a reference file to align all the input files, FindReferenceImage function defines the median file as the reference.		
			"""
			medianFile = FindReferenceImage(centerFiles_segmentations)

			"""
			Apply rigid alignment
			This function can handle both cases (processing only segmentation data or raw and segmentation data at the same time).
			This function uses the same transfrmation matrix for alignment of raw and segmentation files.
			"""
			rigidFiles_segmentations = applyRigidAlignment(groomDir_dm + "aligned", centerFiles_segmentations, None, medianFile,
													   processRaw=False)

			"""Compute largest bounding box and apply cropping"""
			croppedFiles_segmentations = applyCropping(groomDir_dm + "cropped", rigidFiles_segmentations,
													   groomDir_dm + "aligned/*.nrrd")

			print("\nStep 3. Groom - Convert to distance transforms\n")
			if args.interactive:
				input("Press Enter to continue")

			"""
			We convert the scans to distance transforms, this step is common for both the
			prepped as well as unprepped data, just provide correct filenames.
			"""
			dtFiles = applyDistanceTransforms(groomDir_dm, croppedFiles_segmentations)

			copytree(groomDir_dm,groomDir)

			dtFiles = []
			dt_dir = outputDirectory + '/groomed/distance_transforms/'
			for file in sorted(os.listdir(dt_dir)):
				dtFiles.append(dt_dir + file)
	
	
	"""
	## OPTIMIZE : Particle Based Optimization

	Now that we have the distance transform representation of data we create
	the parameter files for the shapeworks particle optimization routine.
	For more details on the plethora of parameters for shapeworks please refer to
	'/Documentation/PDFs/ParameterDescription.pdf'

	We provide two different mode of operations for the ShapeWorks particle opimization;
	1- Single Scale model takes fixed number of particles and performs the optimization.
	2- Multi scale model optimizes for different number of particles in hierarchical manner.

	For more detail about the optimization steps and parameters please refer to
	'/docs/workflow/optimize.md'

	First we need to create a dictionary for all the parameters required by these
	optimization routines
	"""
	print("\nStep 4. Optimize - Particle Based Optimization\n")
	if args.interactive:
		input("Press Enter to continue")

	pointDir = outputDirectory + 'shape_models/'
	if not os.path.exists(pointDir):
		os.makedirs(pointDir)

	parameterDictionary = {
		"number_of_particles": [254, 64, 254],
		"use_shape_statistics_after": 0,
		"use_normals": [0, 0, 0],
		"normal_weight": 1.0,
		"checkpointing_interval" : 10000,
		"keep_checkpoints" : 0,
		"iterations_per_split" : 2000,
		"optimization_iterations" : 500,
		"starting_regularization" : 10,
		"ending_regularization" : 1,
		"recompute_regularization_interval" : 1,
		"domains_per_shape" : 3,
		"domain_type" : 'image',
		"relative_weighting" : 10,
		"initial_relative_weighting" : 1,
		"procrustes_interval" : 1,
		"procrustes_scaling" : 1,
		"save_init_splits" : 0,
		"verbosity" : 2,
		"visualizer_enable": 0,
		"visualizer_wireframe": 0,
		"use_xyz":[1,1,1],
		# "visualizer_screenshot_directory": "screenshots_" + str(use_case) + "_" + str(num_samples) + "samples_" + str(num_particles) + "particles/",
	}

	if args.tiny_test:
		parameterDictionary["number_of_particles"] = 32
		parameterDictionary["optimization_iterations"] = 25
		parameterDictionary["iterations_per_split"] = 25

	if not args.use_single_scale:
		parameterDictionary["use_shape_statistics_after"] = 64

	"""
	Now we execute the particle optimization function.
	"""

	mesh_dir = groomDir + 'reflected/segmentations/'
	vtks = glob.glob(mesh_dir + "*.vtk")

		
	[localPointFiles, worldPointFiles] = runShapeWorksOptimize(pointDir, dtFiles, parameterDictionary)
	#runShapeWorksOptimize(pointDir, files_mesh, parameterDictionary)
	
	if args.tiny_test:
		print("Done with tiny test")
		exit()

	"""
	## ANALYZE : Shape Analysis and Visualization

	Shapeworks yields relatively sparse correspondence models that may be inadequate to reconstruct
	thin structures and high curvature regions of the underlying anatomical surfaces.
	However, for many applications, we require a denser correspondence model, for example,
	to construct better surface meshes, make more detailed measurements, or conduct biomechanical
	or other simulations on mesh surfaces. One option for denser modeling is
	to increase the number of particles per shape sample. However, this approach necessarily
	increases the computational overhead, especially when modeling large clinical cohorts.

	Here we adopt a template-deformation approach to establish an inter-sample dense surface correspondence,
	given a sparse set of optimized particles. To avoid introducing bias due to the template choice, we developed
	an unbiased framework for template mesh construction. The dense template mesh is then constructed
	by triangulating the isosurface of the mean distance transform. This unbiased strategy will preserve
	the topology of the desired anatomy  by taking into account the shape population of interest.
	In order to recover a sample-specific surface mesh, a warping function is constructed using the
	sample-level particle system and the mean/template particle system as control points.
	This warping function is then used to deform the template dense mesh to the sample space.

	Reconstruct the dense mean surface given the sparse correspondence model.
	"""
	
	particleFolder = pointDir+"254/"
	file_type = "local"
	concatenate_particle_files(file_type,3,particleFolder, pointDir)
	command = "ShapeWorksStudio "+ pointDir+ "multi_domain_"+file_type+".xml"
	os.system(command)
	
	#print("\nStep 5. Analysis - Reconstruct the dense mean surface given the sparse correspodence model.\n")
	#if args.interactive:
	#	input("Press Enter to continue")

	#launchShapeWorksStudio(pointDir, dtFiles, localPointFiles, worldPointFiles)
