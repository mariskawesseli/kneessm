import vtk
import os, os.path


def loadStl(fname):
    """Load the given STL file, and return a vtkPolyData object for it."""
    reader = vtk.vtkSTLReader()
    reader.SetFileName(fname)
    reader.Update()
    polydata = reader.GetOutput()

    stlMapper = vtk.vtkPolyDataMapper()
    stlMapper.SetInputConnection(reader.GetOutputPort())

    stlActor = vtk.vtkActor()
    stlActor.SetMapper(stlMapper)

    return polydata, stlActor


def writeply(surface,filename):
    """Write mesh as ply file."""
    writer = vtk.vtkPLYWriter()
    writer.SetInputData(surface)
    writer.SetFileTypeToASCII()
    writer.SetFileName(filename)
    writer.Write()


def scaleSTL(actor, scale=(1000,1000,1000)):
    polyData = vtk.vtkPolyData()
    polyData.DeepCopy(actor.GetMapper().GetInput())
    transform = vtk.vtkTransform()
    transform.SetMatrix(actor.GetMatrix())
    transform.Scale(scale)
    fil = vtk.vtkTransformPolyDataFilter()
    fil.SetTransform(transform)
    fil.SetInputDataObject(polyData)
    fil.Update()
    polyData.DeepCopy(fil.GetOutput())

    return polyData
#
# # stl_file = r"C:\Users\mariskawesseli\Documents\GitLab\ssm\DU_SSM\input_files\MODES\Mean\Geometry\lenhart2015-R-patella-bone.stl"
# # ply_file = r"C:\Users\mariskawesseli\Documents\GitLab\ssm\DU_SSM\input_files\MODES\Mean\Geometry\lenhart2015-R-patella-bone.ply"
# # [stl_mesh, stl_actor] = loadStl(stl_file)
# # stl_file_scale = scaleSTL(stl_actor)
# # writeply(stl_file_scale, ply_file)
# folder = r"C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\Output\femur_patella_tibia\meshes"
# no_files = len([name for name in os.listdir(folder) if os.path.isfile(os.path.join(folder, name))])
#
# for i in range(1, int(no_files/2)):
#     # stl_file = os.path.join(folder,"PAT_"+ str(i)+ ".stl")
#     # ply_file = os.path.join(folder,"PAT_"+ str(i)+ ".ply")
#     #
#     # [stl_mesh, stl_actor] = loadStl(stl_file)
#     # writeply(stl_mesh, ply_file)
#     os.rename(os.path.join(folder,"TIB_"+ str(i)+ ".ply"),os.path.join(folder, str(i) + "_TIB" + ".ply"))

# from glob import glob
# files1 = glob(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\Output\patella_meshes\shape_models_new\64\*world.particles')
# files2 = glob(r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\Output\patella_meshes\shape_models\64\*world.particles')
# for ind in range(0,len(files2)):
#     if os.path.getsize(files1[ind]) == os.path.getsize(files2[ind]):
#         if open(files1[ind],'r').read() == open(files2[ind],'r').read():
#             print(str(ind) + files1[ind] + ' = ' + files2[ind])