import glob
import numpy as np

shapeModelDir = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\fibula_bone\shape_models\2048'

fileList = sorted(glob.glob(shapeModelDir + '/*local.particles'))
for i in range(len(fileList)):
	if i == 0:
		meanShape = np.loadtxt(fileList[i])
	else:
		meanShape += np.loadtxt(fileList[i])
meanShape = meanShape / len(fileList)
nmMS = shapeModelDir + '/meanshape_local.particles'
np.savetxt(nmMS, meanShape)