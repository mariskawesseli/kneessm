import argparse
import os
import sys
import platform
from pathlib import Path
import glob

def setup_shapeworks_env(shapeworks_bin_dir=None,  # path to the binary directory of shapeworks
                         verbose=True):
    # if not set, assume a binary deployment and guess at location
    if shapeworks_bin_dir is None:
        if platform.system() == "Windows":
            shapeworks_bin_dir = "C:\\Program Files\\ShapeWorks\\bin"
        elif platform.system() == "Darwin":  # MacOS
            shapeworks_bin_dir = "/Applications/ShapeWorks/bin"
        else:  # Linux
            shapeworks_bin_dir = "../../../../bin"
    # add shapeworks (and studio on mac) directory to python path
    sys.path.append(shapeworks_bin_dir)
    if platform.system() == "Darwin":  # MacOS
        sys.path.append(shapeworks_bin_dir + "/ShapeWorksStudio.app/Contents/MacOS")

    # add shapeworks and studio to the system path
    os.environ["PATH"] = shapeworks_bin_dir + os.pathsep + os.environ["PATH"]
    if platform.system() == "Darwin":  # MacOS
        os.environ["PATH"] = shapeworks_bin_dir + "/ShapeWorksStudio.app/Contents/MacOS" + os.pathsep + os.environ[
            "PATH"]

def getDistanceGroom(out_dir, DT_dir, prediction_dir):
    # Step 1: Get list of test names
    test_names = []
    for file in os.listdir(prediction_dir):
        test_names.append(getPrefix(file))
    # Step 2: Get original meshes from test distance transforms
    print("\n\nGetting original meshes...")
    DT_files = []
    for file in os.listdir(DT_dir):
        prefix = getPrefix(file)
        if prefix in test_names:
            DT_files.append(DT_dir + '/' + file)
    orig_mesh_list = sorted(GetMeshFromDT(DT_files, out_dir + "OriginalMeshes/"))
    # Step 3: Get predicted meshes from predicted particles
    print("\n\nGetting processed meshes...")
    prediction_files = []
    for file in os.listdir(prediction_dir):
        prefix = getPrefix(file)
        if prefix in test_names:
            prediction_files.append(prediction_dir + '/' + file)

    pred_mesh_list = sorted(GetMeshFromDT(prediction_files, out_dir + "PredcitedMeshes/"))
    # Step 4: Get distance between original and predicted mesh
    print("\n\nGetting distance between original and predicted meshes...")
    orig_dist_mesh_list, pred_dist_mesh_list = surface2surfaceDist(orig_mesh_list, pred_mesh_list, out_dir)
    # Step 5: Get aggregate values regarding distance
    avg_distance = AnalyzeDiffs(orig_dist_mesh_list, pred_dist_mesh_list, out_dir)
    print("Done.\n")
    return avg_distance


if __name__ == '__main__':
    # setup shapeworks environment
    shapeworks_bin_dir = None  # use default
    setup_shapeworks_env(shapeworks_bin_dir, verbose=False)

    from EvaluationUtils import scree_plot, generalization, specificity
    from DeepSSMUtils.Analyze import *
    import subprocess

    # set data directory
    data_dir = r'Output/tibia_bone'
    shape_models_dir = f'{data_dir}/shape_models/1024'
    if not os.path.exists(shape_models_dir):
        print(f'Tibia bone output not found in {shape_models_dir}.',
              file=sys.stderr)
        sys.exit(1)

    eval_dir = f'{data_dir}/evaluation'
    for subdir in ('compactness', 'generalization', 'specificity','meshes'):
        Path(eval_dir).joinpath(Path(subdir)).mkdir(parents=True, exist_ok=True)

    # Get mean shape
    # execCommand = ["ReconstructMeanSurface",
    # 			   os.getcwd() + '/' + data_dir + '/shape_models/analyze.xml',
    # 			   "--out_prefix", 'mean1024'
    # 			   ]
    # subprocess.check_call(execCommand)

    """Differences grooming steps"""
    #reflected vs resampling
    reflected_dir = f'{data_dir}/groomed/reflected/images'
    resampled_dir = f'{data_dir}/groomed/resampled/images'
    out_dir = f'{eval_dir}/meshes/resampled'
    avg_distance = getDistanceGroom(out_dir, reflected_dir, resampled_dir)
    print("Average surface-to-surface distance from the reflected to resampled mesh = " + str(avg_distance))

    # distance transform vs crop
    DT_dir = f'{data_dir}/groomed/distance_transforms/'
    crop_dir = f'{data_dir}/groomed/cropped/images/'
    out_dir = f'{eval_dir}/groom/DT2/'
    avg_distance = getDistanceGroom(out_dir, DT_dir, crop_dir)
    print("Average surface-to-surface distance from the DT to cropped mesh = " + str(avg_distance))

    """Difference distance transform mesh and reconstructed SSM instance"""
    DT_dir = f'{data_dir}/groomed/distance_transforms/'
    out_dir = f'{eval_dir}/meshes/'
    mean_prefix = f'{data_dir}/shape_models/mean1024'
    avg_distance = Analyze.getDistance(out_dir, DT_dir, shape_models_dir + '/', mean_prefix)
    print("Average surface-to-surface distance from the original to predicted shape = " + str(avg_distance))

    """SSM quality measures"""
    # Compute compactness
    scree_plot(glob.glob(f'{shape_models_dir}/*world.particles'), f'{eval_dir}/compactness')

    # Compute generalization
    generalization(glob.glob(f'{shape_models_dir}/*world.particles'), f'{eval_dir}/generalization')

    # Compute specificity
    specificity(glob.glob(f'{shape_models_dir}/*world.particles'), f'{eval_dir}/specificity')