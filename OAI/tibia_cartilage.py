# -*- coding: utf-8 -*-
"""
====================================================================
Full Example Pipeline for Statistical Shape Modeling with ShapeWorks
====================================================================

In this example we provide a full pipeline with optimizing on femur meshes.
"""
import os
import re
import numpy as np


# helper function to add shapeworks bin directory to the path
def setup_shapeworks_env(shapeworks_bin_dir=None,  # path to the binary directory of shapeworks
						 verbose=True):
	# if not set, assume a binary deployment and guess at location
	if shapeworks_bin_dir is None:
		if platform.system() == "Windows":
			shapeworks_bin_dir = "C:\\Program Files\\ShapeWorks\\bin"
		elif platform.system() == "Darwin":  # MacOS
			shapeworks_bin_dir = "/Applications/ShapeWorks/bin"
		else:  # Linux
			shapeworks_bin_dir = "../../../../bin"
	# add shapeworks (and studio on mac) directory to python path
	sys.path.append(shapeworks_bin_dir)
	if platform.system() == "Darwin":  # MacOS
		sys.path.append(shapeworks_bin_dir + "/ShapeWorksStudio.app/Contents/MacOS")

	# add shapeworks and studio to the system path
	os.environ["PATH"] = shapeworks_bin_dir + os.pathsep + os.environ["PATH"]
	if platform.system() == "Darwin":  # MacOS
		os.environ["PATH"] = shapeworks_bin_dir + "/ShapeWorksStudio.app/Contents/MacOS" + os.pathsep + os.environ[
			"PATH"]


def Run_Pipeline(args):
	"""
	# locate data
	"""
	datasetName = args.datasetName
	outputDirectory = args.outputDirectory
	side = args.side
	if not os.path.exists(outputDirectory):
		os.makedirs(outputDirectory)

	"""
	## GROOM : Data Pre-processing
	For the unprepped data the first few steps are
	-- Reflect images and meshes
	-- Turn meshes to volumes
	-- Isotropic resampling
	-- Padding
	-- Center of Mass Alignment
	-- Centering
	-- Rigid Alignment
	-- if interactive tag and option 2 was chosen - define cutting plane on mean sample
	-- clip segementations with cutting plane
	-- find largest bounding box and crop
	"""
	# Directory where grooming output folders will be added
	groomDir = outputDirectory + 'groomed/'
	if not os.path.exists(groomDir):
		os.makedirs(groomDir)

	# set name specific variables
	img_suffix = "1x_pat"
	reference_side = "right"

	# Get mesh segmentation file lists
	inputDir = args.inputDir
	files_mesh = []
	files_seg = []
	mesh_dir = inputDir + r'/segmentation_meshes/' + datasetName + '/mesh/' + side + '/'
	for file in sorted(os.listdir(mesh_dir)):
		files_mesh.append(mesh_dir + file)
	seg_dir = inputDir + r'/segmentation_meshes/' + datasetName + '/segmentation/' + side + '/'
	for file in sorted(os.listdir(seg_dir)):
		files_seg.append(seg_dir + file)

	# use 3 sample if running a tiny test
	if args.tiny_test:
		files_mesh = files_mesh[:3]
		files_seg = files_seg[:3]
		args.use_single_scale = True
		args.interactive = False

	# run clustering if running on a subset
	if args.use_subsample:
		# sample_idx = sampledata(files_seg, int(args.use_subsample))
		# files_mesh = [files_seg[i] for i in sample_idx]
		# files_seg = [files_mesh[i] for i in sample_idx]
		# files_mesh = files_mesh[:int(args.use_subsample)]
		# files_seg = files_seg[:int(args.use_subsample)]
		matches_mesh = []
		matches_seg = []
		pts_use = args.use_subsample
		for pt in pts_use:
			if any(pt in s for s in files_mesh):
				matches_mesh.append([match for match in files_mesh if pt in match])
			if any(pt in s for s in files_seg):
				matches_seg.append([match for match in files_seg if pt in match])
		files_mesh = [item for sublist in matches_mesh for item in sublist]
		files_seg = [item for sublist in matches_seg for item in sublist]

	if args.start_with_image_and_segmentation_data:
		print("\n\n************************ WARNING ************************")
		print("'start_with_image_and_segmentation_data' tag was used \nbut  data set does not have images.")
		print("Continuing to run with segmentations only.")
		print("*********************************************************\n\n")

	if int(args.start_with_prepped_data) == 1:
		print("Skipping grooming...")
		dtFiles = []
		dt_dir = outputDirectory + r'/groomed/distance_transforms/'
		for file in sorted(os.listdir(dt_dir)):
			dtFiles.append(dt_dir + file)

		if args.use_subsample:
			dtFiles = dtFiles[:int(args.use_subsample)]

		if args.tiny_test:
			dtFiles = dtFiles[:3]

	else:
		# """
		# 	   Reflect - We have left and right femurs, so we reflect both image and mesh
		# 	   for the non-reference side so that all of the femurs can be aligned.
		# 	   """
		# reflectedFiles_mesh, reflectedFile_img = anatomyPairsToSingles(groomDir + 'reflected', files_mesh, files_seg,
		# 															   reference_side)
		#
		# """
		# 		   MeshesToVolumes - Shapeworks requires volumes so we need to convert
		# 		   mesh segementaions to binary segmentations.
		# 		   """
		# # set spacing
		# spacing = [0.69999800000000001, 0.36458299999999999, 0.36458299999999999]  #[0.5,0.5,0.5] #
		# # fileList_seg = MeshesToVolumes(groomDir + "volumes", reflectedFiles_mesh, spacing)
		# fileList_seg = MeshesToVolumesUsingImages(groomDir + "volumes", reflectedFiles_mesh, reflectedFile_img)
		#
		# """
		# Apply isotropic resampling
		# The segmentation and images are resampled independently to have uniform spacing.
		# """
		# resampledFiles_segmentations = applyIsotropicResampling(groomDir + "resampled/segmentations", fileList_seg,
		# 														isoSpacing=1, isBinary=True)
		# resampledFiles_images = applyIsotropicResampling(groomDir + "resampled/images", reflectedFile_img,
		# 												 isoSpacing=1, isBinary=False)
		#
		# """
		# Apply padding
		# Both the segmentation and raw images are padded in case the seg lies on the image boundary.
		# """
		# paddedFiles_segmentations = applyPadding(groomDir + "padded/segementations", resampledFiles_segmentations, 30)
		# paddedFiles_images = applyPadding(groomDir + "padded/images", resampledFiles_images, 30)

		# """
		# Apply center of mass alignment
		# This function can handle both cases (processing only segmentation data or raw and segmentation data at the same time).
		# """
		# paddedFiles_segmentations = []
		# dir_center = groomDir + "padded/segementations/"
		# for file in sorted(os.listdir(dir_center)):
		#     paddedFiles_segmentations.append(dir_center + file)
		# paddedFiles_images = []
		# dir_center = groomDir + "padded/images/"
		# for file in sorted(os.listdir(dir_center)):
		# 	paddedFiles_images.append(dir_center + file)

		# comFiles_segmentations = applyCOMAlignment(groomDir + "com_aligned", paddedFiles_segmentations, None)
		# [comFiles_segmentations, comFiles_images] = applyCOMAlignment(groomDir + "com_aligned",
		# 															  paddedFiles_segmentations, paddedFiles_images,
		# 															  processRaw=True, Tmat=True)
		# """
		# Apply centering
		# """
		# centerFiles_segmentations = center(groomDir + "centered/segmentations", comFiles_segmentations)
		# centerFiles_images = center(groomDir + "centered/images", comFiles_images)
		#
		# # centerFiles_segmentations = []
		# # dir_center = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\tibia_cartilage_lat\groomed\centered/'
		# # for file in sorted(os.listdir(dir_center + 'segmentations/')):
		# #     centerFiles_segmentations.append(dir_center + 'segmentations/' + file)
		# # centerFiles_segmentations = centerFiles_segmentations[:args.use_subsample]
		# # centerFiles_images = []
		# # dir_center = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\tibia_cartilage_lat\groomed\centered/'
		# # for file in sorted(os.listdir(dir_center + 'images/')):
		# # 	centerFiles_images.append(dir_center + 'images/' + file)
		# # centerFiles_images = centerFiles_images[:args.use_subsample]
		# """
		# Rigid alignment needs a reference file to align all the input files, FindReferenceImage function defines the median file as the reference.
		# """
		# medianFile = FindReferenceImage(centerFiles_segmentations,groomDir,fixed=True)
		#
		# """
		# Apply rigid alignment
		# This function can handle both cases (processing only segmentation data or raw and segmentation data at the same time).
		# This function uses the same transfrmation matrix for alignment of raw and segmentation files.
		# """
		# # rigidFiles_segmentations = applyRigidAlignment(groomDir + "aligned", centerFiles_segmentations, None, medianFile,
		# #                                            processRaw=False)
		# # rigidFiles_segmentations = applyRigidAlignment(groomDir + "aligned", medianFile, centerFiles_segmentations)
		# aligned_segmentations, aligned_images = applyRigidAlignment(groomDir + "aligned", medianFile,
		# 															centerFiles_segmentations, centerFiles_images, Tmat=True)
		# # rigidFiles = applyRigidAlignment(groomDir + "aligned/segmentations", comFiles, None, comFiles[0])
		#
		# # aligned_images = []
		# # dir_center = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\tibia_cartilage_lat\groomed\aligned/'
		# # for file in sorted(os.listdir(dir_center + 'images/')):
		# #     aligned_images.append(dir_center + 'images/' + file)
		# # aligned_images = aligned_images[:args.use_subsample]
		#
		# input_file = medianFile.replace("centered", "aligned").replace(".nrrd", ".aligned.nrrd")
		# cutting_plane_points = SelectCuttingPlaneML(input_file)
		# #
		# print("Cutting plane points: ")
		# print(cutting_plane_points)
		# cutting_plane_points[0][0] = cutting_plane_points[0][0] * -1
		# cutting_plane_points[1][0] = cutting_plane_points[1][0] * -1
		# cutting_plane_points[2][0] = cutting_plane_points[2][0] * -1
		#
		# """
		#             Clip Binary Volumes - We have femurs of different shaft length so we will clip them all using the defined cutting plane.
		#             """
		# # clippedFiles_segmentations = ClipBinaryVolumes(groomDir + 'clipped_segmentations', aligned_segmentations,
		# #                                                cutting_plane_points.flatten())
		# clippedFiles_images = ClipBinaryVolumes(groomDir + 'clipped_images', aligned_images,
		#                                                cutting_plane_points.flatten())
		#
		# """Compute largest bounding box and apply cropping"""
		clippedFiles_images = []
		dir_center = groomDir + "clipped_images/"
		for file in sorted(os.listdir(dir_center)):
		    clippedFiles_images.append(dir_center + file)
		# # croppedFiles_segmentations = applyCropping(groomDir + "cropped/segmentations", rigidFiles_segmentations,
		# #                                            groomDir + "aligned/*.nrrd")
		croppedFiles_images = applyCropping(groomDir + "cropped/images", clippedFiles_images, clippedFiles_images,paddingSize=30)

		# croppedFiles_segmentations = applyCropping(groomDir + "cropped/segmentations", clippedFiles_segmentations,
		#                                            clippedFiles_segmentations)
		# croppedFiles_images = applyCropping(groomDir + "cropped/images", clippedFiles_images, clippedFiles_images)
		#
		groomed_segmentations = croppedFiles_images #croppedFiles_images  #croppedFiles_segmentations #

		print("\nStep 3. Groom - Convert to distance transforms\n")
		if args.interactive:
			input("Press Enter to continue")

		"""
		We convert the scans to distance transforms, this step is common for both the
		prepped as well as unprepped data, just provide correct filenames.
		"""
		# dtFiles = applyDistanceTransforms(groomDir, croppedFiles_segmentations)
		dtFiles = applyDistanceTransforms(groomDir, groomed_segmentations, antialiasIterations=0, smoothingIterations=1, alpha=10.5, beta=10.0, scaling=20.0)

	"""
	## OPTIMIZE : Particle Based Optimization

	Now that we have the distance transform representation of data we create
	the parameter files for the shapeworks particle optimization routine.
	For more details on the plethora of parameters for shapeworks please refer to
	'/Documentation/PDFs/ParameterDescription.pdf'

	We provide two different mode of operations for the ShapeWorks particle opimization;
	1- Single Scale model takes fixed number of particles and performs the optimization.
	2- Multi scale model optimizes for different number of particles in hierarchical manner.

	For more detail about the optimization steps and parameters please refer to
	'/docs/workflow/optimize.md'

	First we need to create a dictionary for all the parameters required by these
	optimization routines
	"""
	print("\nStep 4. Optimize - Particle Based Optimization\n")
	if args.interactive:
		input("Press Enter to continue")

	pointDir = outputDirectory + 'shape_models/'
	if not os.path.exists(pointDir):
		os.makedirs(pointDir)

	parameterDictionary = {
		"number_of_particles": 1024,
		"use_shape_statistics_after": 0,
		"use_normals": 1,
		"normal_weight": 5.0,
		"checkpointing_interval" : 10000,
		"keep_checkpoints" : 0,
		"iterations_per_split" : 4000,
		"optimization_iterations" : 500,
		"starting_regularization" : 1,
		"ending_regularization" : 0.1,
		"recompute_regularization_interval" : 1,
		"domains_per_shape" : 1,
		"domain_type" : 'image',
		"relative_weighting" : 1,
		"initial_relative_weighting" : 0.1,
		"procrustes_interval" : 1,
		"procrustes_scaling" : 3,
		"save_init_splits" : 0,
		"verbosity" : 2,
		"visualizer_enable": 0,
		"visualizer_wireframe": 0,
		"narrow_band": 10,
		# "visualizer_screenshot_directory": "screenshots_" + str(use_case) + "_" + str(num_samples) + "samples_" + str(num_particles) + "particles/",
	}

	if args.tiny_test:
		parameterDictionary["number_of_particles"] = 32
		parameterDictionary["optimization_iterations"] = 25
		parameterDictionary["iterations_per_split"] = 25

	if not args.use_single_scale:
		parameterDictionary["use_shape_statistics_after"] = 64

	"""
	Now we execute the particle optimization function.
	"""
	[localPointFiles, worldPointFiles] = runShapeWorksOptimize(pointDir, dtFiles, parameterDictionary)

	if args.tiny_test:
		print("Done with tiny test")
		# exit()

	"""
	## ANALYZE : Shape Analysis and Visualization

	Shapeworks yields relatively sparse correspondence models that may be inadequate to reconstruct
	thin structures and high curvature regions of the underlying anatomical surfaces.
	However, for many applications, we require a denser correspondence model, for example,
	to construct better surface meshes, make more detailed measurements, or conduct biomechanical
	or other simulations on mesh surfaces. One option for denser modeling is
	to increase the number of particles per shape sample. However, this approach necessarily
	increases the computational overhead, especially when modeling large clinical cohorts.

	Here we adopt a template-deformation approach to establish an inter-sample dense surface correspondence,
	given a sparse set of optimized particles. To avoid introducing bias due to the template choice, we developed
	an unbiased framework for template mesh construction. The dense template mesh is then constructed
	by triangulating the isosurface of the mean distance transform. This unbiased strategy will preserve
	the topology of the desired anatomy  by taking into account the shape population of interest.
	In order to recover a sample-specific surface mesh, a warping function is constructed using the
	sample-level particle system and the mean/template particle system as control points.
	This warping function is then used to deform the template dense mesh to the sample space.

	Reconstruct the dense mean surface given the sparse correspondence model.
	"""
	print("\nStep 5. Analysis - Reconstruct the dense mean surface given the sparse correspodence model.\n")
	if args.interactive:
		input("Press Enter to continue")

	launchShapeWorksStudio(pointDir, dtFiles, localPointFiles, worldPointFiles)

if __name__ == '__main__':
	import os
	import sys
	import platform

	shapeworks_bin_dir = None  # use default

	# To override, uncomment and set paths here
	# shapeworks_bin_dir   = "../../../../build/bin"

	# set up shapeworks environment
	setup_shapeworks_env(shapeworks_bin_dir, verbose=False)

	# let's import shapeworks library to test whether shapeworks is now set
	# if the error is not printed, we are done with the setup
	# print_python_path()

	try:
		import shapeworks as sw
	except ImportError:
		print('ERROR: shapeworks library failed to import')
	else:
		print('SUCCESS: shapeworks library is successfully imported!!!')

	from GroomUtils import *
	from OptimizeUtils import *
	from AnalyzeUtils import *
	import CommonUtils
	import argparse

	pt_to_use = r'C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\healthyKL_pts.txt'
	with open(pt_to_use) as f:
		pts = f.readlines()
	pts = [i.split('\n')[0] for i in pts]

	# CommonUtils.robustifyShapeworksPaths()
	args = argparse.ArgumentParser(description='Tibia cartialge Pipeline')
	args.tiny_test = False
	args.use_subsample = True
	args.use_subsample = pts
	args.start_with_image_and_segmentation_data = False
	args.start_with_prepped_data = False
	args.shapeworks_path = r"C:\Program Files\ShapeWorks\bin"
	explicit_binpath = args.shapeworks_path
	default_binpath = "C:\\Program Files\\ShapeWorks\\bin"
	os.environ["PATH"] = explicit_binpath + os.pathsep + os.environ["PATH"] + os.pathsep + default_binpath
	args.interactive = False
	args.use_single_scale = True
	args.datasetName = "tibia_cartilage"
	args.side = 'med'  # 'lat' #
	args.outputDirectory = r'C:/Users/mariskawesseli/Documents/GitLab/knee_ssm/OAI/Output/tibia_cartilage_' + args.side + '/'
	args.start_with_prepped_data = 0
	args.inputDir = r'C:/Users/mariskawesseli/Documents/Data/OAI/segmentation/2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB/OAI-ZIB/segmentation'


	Run_Pipeline(args)
