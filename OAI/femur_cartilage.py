# -*- coding: utf-8 -*-
"""
====================================================================
Full Example Pipeline for Statistical Shape Modeling with ShapeWorks
====================================================================

In this example we provide a full pipeline with optimizing on femur meshes.
"""
import os
import re
import numpy as np
import glob
import glob

# helper function to add shapeworks bin directory to the path
def setup_shapeworks_env(shapeworks_bin_dir=None,  # path to the binary directory of shapeworks
                         verbose=True):
    # if not set, assume a binary deployment and guess at location
    if shapeworks_bin_dir is None:
        if platform.system() == "Windows":
            shapeworks_bin_dir = "C:\\Program Files\\ShapeWorks\\bin"
        elif platform.system() == "Darwin":  # MacOS
            shapeworks_bin_dir = "/Applications/ShapeWorks/bin"
        else:  # Linux
            shapeworks_bin_dir = "../../../../bin"
    # add shapeworks (and studio on mac) directory to python path
    sys.path.append(shapeworks_bin_dir)
    if platform.system() == "Darwin":  # MacOS
        sys.path.append(shapeworks_bin_dir + "/ShapeWorksStudio.app/Contents/MacOS")

    # add shapeworks and studio to the system path
    os.environ["PATH"] = shapeworks_bin_dir + os.pathsep + os.environ["PATH"]
    if platform.system() == "Darwin":  # MacOS
        os.environ["PATH"] = shapeworks_bin_dir + "/ShapeWorksStudio.app/Contents/MacOS" + os.pathsep + os.environ[
            "PATH"]


def Run_Pipeline(args):
    """
    # locate data
    """
    datasetName = args.datasetName
    outputDirectory = args.outputDirectory
    if not os.path.exists(outputDirectory):
        os.makedirs(outputDirectory)

    """
    ## GROOM : Data Pre-processing
    For the unprepped data the first few steps are
    -- Reflect images and meshes
    -- Turn meshes to volumes
    -- Isotropic resampling
    -- Padding
    -- Center of Mass Alignment
    -- Centering
    -- Rigid Alignment
    -- if interactive tag and option 2 was chosen - define cutting plane on mean sample
    -- clip segementations with cutting plane
    -- find largest bounding box and crop
    """
    # Directory where grooming output folders will be added
    groomDir = outputDirectory + 'groomed/'
    if not os.path.exists(groomDir):
        os.makedirs(groomDir)

    # set name specific variables
    img_suffix = "1x_pat"
    reference_side = "right"

    # Get mesh segmentation file lists
    inputDir = args.inputDir
    files_mesh = []
    files_seg = []
    mesh_dir = inputDir + r'/segmentation_meshes/' + datasetName + '/mesh/'
    for file in sorted(os.listdir(mesh_dir)):
        files_mesh.append(mesh_dir + file)
    seg_dir = inputDir + r'/segmentation_meshes/' + datasetName + '/segmentation/'
    for file in sorted(os.listdir(seg_dir)):
        files_seg.append(seg_dir + file)

    # use 3 sample if running a tiny test
    if args.tiny_test:
        files_mesh = files_mesh[:3]
        files_seg = files_seg[:3]
        args.use_single_scale = True
        args.interactive = False

    # run clustering if running on a subset
    if args.use_subsample:
        # sample_idx = sampledata(files_seg, int(args.use_subsample))
        # files_mesh = [files_seg[i] for i in sample_idx]
        # files_seg = [files_mesh[i] for i in sample_idx]
        # files_mesh = files_mesh[:int(args.use_subsample)]
        # files_seg = files_seg[:int(args.use_subsample)]
        matches_mesh = []
        matches_seg = []
        pts_use = args.use_subsample
        for pt in pts_use:
            if any(pt in s for s in files_mesh):
                matches_mesh.append([match for match in files_mesh if pt in match])
            if any(pt in s for s in files_seg):
                matches_seg.append([match for match in files_seg if pt in match])
        files_mesh = [item for sublist in matches_mesh for item in sublist]
        files_seg = [item for sublist in matches_seg for item in sublist]

    if args.start_with_image_and_segmentation_data:
        print("\n\n************************ WARNING ************************")
        print("'start_with_image_and_segmentation_data' tag was used \nbut  data set does not have images.")
        print("Continuing to run with segmentations only.")
        print("*********************************************************\n\n")

    if int(args.start_with_prepped_data) == 1:
        print("Skipping grooming...")
        dtFiles = []
        dt_dir = outputDirectory + r'/groomed/distance_transforms/'
        for file in sorted(os.listdir(dt_dir)):
            dtFiles.append(dt_dir + file)

        if args.tiny_test:
            dtFiles = dtFiles[:3]

    else:
        print("\nStep 2. Groom - Data Pre-processing\n")
        """
        Step 2: GROOMING 
        The required grooming steps are: 
        1. Remesh 
        2. Reference selection
        3. Rigid Alignment
        For more information on grooming see docs/workflow/groom.md
        http://sciinstitute.github.io/ShapeWorks/workflow/groom.html
        """

        # Create a directory for groomed output
        groom_dir = outputDirectory + 'groomed/'
        if not os.path.exists(groom_dir):
            os.makedirs(groom_dir)

        """
        First, we need to loop over the mesh files and load them
        """
        mesh_list = []
        mesh_names = []
        for mesh_file in files_mesh:
            print('Loading: ' + mesh_file)
            # get current name
            mesh_names.append(mesh_file.split('/')[-1].replace('.vtk', ''))
            """
            Grooming Step 1: load mesh and remesh
            """
            mesh = sw.Mesh(mesh_file).remeshPercent(percentage=99, adaptivity=1.0)
            # append to the list
            mesh_list.append(mesh)

        """
        Grooming Step 2: Select a reference
        This step requires loading all of the meshes at once so the shape
        closest to the mean can be found and selected as the reference. 
        """
        ref_index = sw.find_reference_mesh_index(mesh_list)
        # Make a copy of the reference segmentation
        ref_mesh = mesh_list[ref_index]
        # Center the reference mesh at 0,0,0
        ref_mesh.translate(-ref_mesh.center()).write(groom_dir + 'reference.vtk')
        ref_name = mesh_names[ref_index]
        print("Reference found: " + ref_name)

        """
        Grooming Step 3: Rigid alignment
        This step rigidly aligns each shape to the selected reference. 
        """
        for mesh, name in zip(mesh_list, mesh_names):
            print('Aligning ' + name + ' to ' + ref_name)
            # compute rigid transformation
            rigid_transform = mesh.createTransform(ref_mesh, sw.Mesh.AlignmentType.Rigid, 100)
            # apply rigid transform
            mesh.applyTransform(rigid_transform)

        # Save groomed meshes
        mesh_files = sw.utils.save_meshes(groom_dir + 'meshes/', mesh_list, mesh_names, extension='vtk')

    """
    ## OPTIMIZE : Particle Based Optimization

    Now that we have the distance transform representation of data we create
    the parameter files for the shapeworks particle optimization routine.
    For more details on the plethora of parameters for shapeworks please refer to
    '/Documentation/PDFs/ParameterDescription.pdf'

    We provide two different mode of operations for the ShapeWorks particle opimization;
    1- Single Scale model takes fixed number of particles and performs the optimization.
    2- Multi scale model optimizes for different number of particles in hierarchical manner.

    For more detail about the optimization steps and parameters please refer to
    '/docs/workflow/optimize.md'

    First we need to create a dictionary for all the parameters required by these
    optimization routines
    """
    print("\nStep 4. Optimize - Particle Based Optimization\n")
    if args.interactive:
        input("Press Enter to continue")

    pointDir = outputDirectory + 'shape_models/'
    if not os.path.exists(pointDir):
        os.makedirs(pointDir)

    parameterDictionary = {
        "number_of_particles": 1024,
        "use_shape_statistics_after": 0,
        "use_normals": 0,
        "normal_weight": 1.0,
        "checkpointing_interval" : 10000,
        "keep_checkpoints" : 0,
        "iterations_per_split" : 4000,
        "optimization_iterations" : 500,
        "starting_regularization" : 10,
        "ending_regularization" : 1,
        "recompute_regularization_interval" : 1,
        "domains_per_shape" : 1,
        "domain_type" : 'image',
        "relative_weighting" : 10,
        "initial_relative_weighting" : 1,
        "procrustes_interval" : 1,
        "procrustes_scaling" : 1,
        "save_init_splits" : 0,
        "verbosity" : 2,
        "visualizer_enable": 0,
        "visualizer_wireframe": 0,
        "narrow_band": 10,
        # "visualizer_screenshot_directory": "screenshots_" + str(use_case) + "_" + str(num_samples) + "samples_" + str(num_particles) + "particles/",
    }

    if args.tiny_test:
        parameterDictionary["number_of_particles"] = 32
        parameterDictionary["optimization_iterations"] = 25
        parameterDictionary["iterations_per_split"] = 25

    if not args.use_single_scale:
        parameterDictionary["use_shape_statistics_after"] = 64

    """
    Now we execute the particle optimization function.
    """
    [localPointFiles, worldPointFiles] = runShapeWorksOptimize(pointDir, mesh_files, parameterDictionary)

    if args.tiny_test:
        print("Done with tiny test")
        # exit()

    """
    ## ANALYZE : Shape Analysis and Visualization

    Shapeworks yields relatively sparse correspondence models that may be inadequate to reconstruct
    thin structures and high curvature regions of the underlying anatomical surfaces.
    However, for many applications, we require a denser correspondence model, for example,
    to construct better surface meshes, make more detailed measurements, or conduct biomechanical
    or other simulations on mesh surfaces. One option for denser modeling is
    to increase the number of particles per shape sample. However, this approach necessarily
    increases the computational overhead, especially when modeling large clinical cohorts.

    Here we adopt a template-deformation approach to establish an inter-sample dense surface correspondence,
    given a sparse set of optimized particles. To avoid introducing bias due to the template choice, we developed
    an unbiased framework for template mesh construction. The dense template mesh is then constructed
    by triangulating the isosurface of the mean distance transform. This unbiased strategy will preserve
    the topology of the desired anatomy  by taking into account the shape population of interest.
    In order to recover a sample-specific surface mesh, a warping function is constructed using the
    sample-level particle system and the mean/template particle system as control points.
    This warping function is then used to deform the template dense mesh to the sample space.

    Reconstruct the dense mean surface given the sparse correspondence model.
    """
    print("\nStep 5. Analysis - Reconstruct the dense mean surface given the sparse correspodence model.\n")
    if args.interactive:
        input("Press Enter to continue")

    launchShapeWorksStudio(pointDir, dtFiles, localPointFiles, worldPointFiles)

if __name__ == '__main__':
    import os
    import sys
    import platform

    shapeworks_bin_dir = None  # use default

    # To override, uncomment and set paths here
    # shapeworks_bin_dir   = "../../../../build/bin"

    # set up shapeworks environment
    setup_shapeworks_env(shapeworks_bin_dir, verbose=False)

    # let's import shapeworks library to test whether shapeworks is now set
    # if the error is not printed, we are done with the setup
    # print_python_path()

    try:
        import shapeworks as sw
    except ImportError:
        print('ERROR: shapeworks library failed to import')
    else:
        print('SUCCESS: shapeworks library is successfully imported!!!')

    from GroomUtils import *
    from OptimizeUtils import *
    from AnalyzeUtils import *
    import CommonUtils
    import argparse

    pt_to_use = r'C:\Users\mariskawesseli\Documents\Data\OAI\segmentation\2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB\healthyKL_pts.txt'
    with open(pt_to_use) as f:
        pts = f.readlines()
    pts = [i.split('\n')[0] for i in pts]

    # CommonUtils.robustifyShapeworksPaths()
    args = argparse.ArgumentParser(description='Femur bone Pipeline')
    args.tiny_test = False
    args.use_subsample = True
    args.use_subsample = pts
    args.start_with_image_and_segmentation_data = False
    args.start_with_prepped_data = True
    args.shapeworks_path = r"C:\Program Files\ShapeWorks\bin"
    explicit_binpath = args.shapeworks_path
    default_binpath = "C:\\Program Files\\ShapeWorks\\bin"
    os.environ["PATH"] = explicit_binpath + os.pathsep + os.environ["PATH"] + os.pathsep + default_binpath
    args.interactive = False
    args.use_single_scale = True
    args.datasetName = "femur_cartilage"
    args.outputDirectory = r'C:/Users/mariskawesseli/Documents/GitLab/knee_ssm/OAI/Output/femur_cartilage/'
    args.start_with_prepped_data = 0
    args.inputDir = r'C:/Users/mariskawesseli/Documents/Data/OAI/segmentation/2019_ATEZ_MEDIA-Supplementary-Material-OAI-ZIB/OAI-ZIB/segmentation'

    Run_Pipeline(args)