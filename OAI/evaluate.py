from EvaluationUtils import *

"""Create evaluation plots - generalization, specificity and compactness"""


def plot_generalization(working_dir,fig):
    # Load scree plot data
    Y = np.load(f'{working_dir}/generalization.npy')
    N = len(Y)
    X = np.arange(1, N + 1)

    # Plot data
    plt.rc('font', size=20)
    plt.plot(X, Y, linewidth=4.0)

    fig.set_size_inches(10, 10)
    plt.title('Generalization')
    plt.xlabel('Number of modes')
    plt.ylabel('G(M)(mm)')
    plt.xticks(X)
    plt.ylim(bottom=0, top=1.0)
    plt.xlim(left=1, right=N)
    plt.xticks(np.arange(0, N + 1, 20.0))
    plt.grid()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(4, 3)
    plt.subplots_adjust(left=0.24,
                        bottom=0.27)
    plt.savefig(f'{working_dir}/generalization.png')


def plot_specificity(working_dir, fig, file):
    # Load scree plot data
    Y = np.load(file)
    N = len(Y)
    X = np.arange(1, N + 1)

    # Plot data
    plt.rc('font', size=20)
    plt.plot(X, Y, linewidth=4.0)

    fig.set_size_inches(10, 10)
    plt.title('Specificity')
    plt.xlabel('Number of modes')
    plt.ylabel('S(M)(mm)')
    plt.xticks(X)
    plt.ylim(bottom=0, top=1.0)
    plt.xlim(left=1, right=N)
    plt.xticks(np.arange(0, N + 1, 20.0))
    plt.grid()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(4, 3)
    plt.subplots_adjust(left=0.24,
                        bottom=0.27)
    # plt.savefig(f'{working_dir}/specificity.png')


def plot_compactness(working_dir,fig):
    # Load scree plot data
    Y = np.loadtxt(f'{working_dir}/scree.txt')
    N = len(Y)
    X = np.arange(1, N + 1)

    plt.rc('font', size=20)

    # Plot data
    plt.plot(X, Y, linewidth=4.0)

    fig.set_size_inches(10, 10)
    plt.title('Compactness')
    plt.xlabel('Number of modes')
    plt.ylabel('% of variance')
    plt.xticks(X)
    plt.ylim(bottom=0, top=1.0)
    plt.xlim(left=1, right=N)
    plt.xticks(np.arange(0, N + 1, 20.0))
    plt.grid()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(4, 3)
    plt.subplots_adjust(left=0.24,
                        bottom=0.27)
    plt.savefig(f'{working_dir}/compactness.png')


main_dir = r'C:\Users\mariskawesseli\Documents\GitLab\knee_ssm\OAI\Output\femur_bone'
shape_models_dir = f'{main_dir}/shape_models/8192'

eval_dir = f'{main_dir}/evaluation_8192'

# compactness
# fig = matplotlib.pyplot.gcf()
# plot_compactness(f'{eval_dir}/compactness',fig)
# plt.show()
# Ycomp = np.loadtxt(f'{eval_dir}/compactness/scree.txt')
# Ncomp = len(Ycomp)
# Xcomp = np.arange(1, Ncomp + 1)

# generalization
# fig = matplotlib.pyplot.gcf()
# plot_generalization(f'{eval_dir}/generalization',fig)
# plt.show()
#
# Ygen = np.load(f'{eval_dir}/generalization/generalization.npy')
# Ngen = len(Ygen)
# Xgen = np.arange(1, Ngen + 1)

# specificity
# fig = matplotlib.pyplot.gcf()
# plot_specificity(f'{eval_dir}/specificity',fig, f'{eval_dir}/specificity/specificity new.npy')
# plt.show()
# Yspec = np.load(f'{eval_dir}/specificity/specificity new.npy')
# Nspec = len(Yspec)
# Xspec = np.arange(1, Nspec + 1)

fig = matplotlib.pyplot.gcf()
# plot_specificity(f'{eval_dir}/specificity',fig, f'{eval_dir}/specificity/specificity.txt')
plt.show()
Y = np.loadtxt(f'{eval_dir}/specificity/specificity.txt')
N = len(Y)
X = np.arange(1, N + 1)

# Plot data
plt.rc('font', size=20)
plt.plot(X, Y, linewidth=4.0)

fig.set_size_inches(10, 10)
plt.title('Specificity')
plt.xlabel('Number of modes')
plt.ylabel('S(M)(mm)')
plt.xticks(X)
plt.ylim(bottom=0, top=1.0)
plt.xlim(left=1, right=N)
plt.xticks(np.arange(0, N + 1, 20.0))
plt.grid()
fig = matplotlib.pyplot.gcf()
fig.set_size_inches(4, 3)
plt.subplots_adjust(left=0.24,
                    bottom=0.27)
plt.savefig(f'{eval_dir}/specificity/specificity.png')