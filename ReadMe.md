# Workflow to create statistical shape models (SSM) for the femur, tibia and fibula
--- 
 
 The SSM are created using ZIB OAI dataset. Ligament attachment regions are identified using a CT dataset with attachment regions identified

 Statistical shape models (SSMs) were created by applying principal component analysis using Shapeworks 6.1.0 (shapeworks.sci.utah.edu), a particle-based shape modelling software application.
 
 The code for pre-processing and processing is based on example files from Shapeworks. It requires the full ShapeWorks to be correctly built/downloaded.

 Data is not included in the repository.
 

---
## Installation

 It requires the full ShapeWorks to be correctly built/downloaded, see https://github.com/SCIInstitute/ShapeWorks for more information. 
  

--- 
## Usage 
 
 Pre-processing includes:
 - Reflect images and meshes
 - Turn meshes to volumes
 - Isotropic resampling
 - Padding
 - Center of Mass Alignment
 - Centering
 - Rigid Alignment
 - Define cutting plane on mean sample
 - Clip segementations with cutting plane
 - Find largest bounding box and crop

To create the SSM use:
 - femur_bone.py
 - tibia_bone.py
 - fibula_bone.py

Groom the bone of a new participant not used in the training set 
to determine the SSM modes to reconstruct the bone:
 - groom_femur.py
 - groom_tibia.py
 - groom fibula.py

Reflect bone if left (add _L in the name) as SSM is for right bones. 
Make sure the bone is correctly aligned and cropped as the input bones of the original SSM.
For cropping use the median bone of the original SSM
If needed adapt the padding

Create the mean shape particle file of the original SSM (getMeanShape.py)

Run the SSM with fixed domains, where the number of fixed domains is the number of original input bones used for the SSM.
If needed increase the narrow band.


---
## References
 Voskuijl, T., Wesseling, M., Pennings, M., Piscaer, T., Hanff, D., Meuffels, D.E. "The adaption of anterior and posterior cruciate ligament attachment sites to the variance of three dimensional bony knee shapes". Submitted to XX.

 Use this link to show the interactive figures for the mean SSM with ligament attachment regions for the ACL and PCL: https://nbviewer.org/github/mariska-wesseling/ACLandPCLattachments/blob/17a8e9e9680f5e648cc5b21e47c13403af7fff3c/VisualizeSSM.ipynb

 Use this link to show the interactive figures for the mean SSM with ligament attachment regions for the lateral ligaments: https://nbviewer.org/github/mariska-wesseling/LateralLigamentAttachments/blob/main/VisualizeSSM_lateral.ipynb

---
## License
 https://creativecommons.org/licenses/by/4.0/
 
 